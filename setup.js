let enabledIcon = {
    path: {
        48: "/icons/tor-48.png",
        96: "/icons/tor-96.png"
    }
};

let disabledIcon = {
    path: {
        48: "/icons/tor-48-gray.png",
        96: "/icons/tor-96-gray.png"
    }
};

window.onload = function () {
    browser.storage.local.get("enabled").then(onLoadConfig, onError);
};

function onLoadConfig(config) {
    browser.browserAction.setIcon(config.enabled ? enabledIcon : disabledIcon);
}

function onError() {
    console.error("The extension's configuration could not be loaded");
}
