let proxySettings = {
    proxyType: "manual",
    socks: "127.0.0.1:9050",
    socksVersion: 5
}

let enabledIcon = {
    path: {
        48: "/icons/tor-48.png",
        96: "/icons/tor-96.png"
    }
};

let disabledIcon = {
    path: {
        48: "/icons/tor-48-gray.png",
        96: "/icons/tor-96-gray.png"
    }
};

let enableTorCheckbox = document.getElementById("enable-tor");
let addressInput = document.getElementById("address");
let portInput = document.getElementById("port");

function loadConfig() {
    browser.storage.local.get({
        enabled: false,
        proxyAddress: "127.0.0.1",
        proxyPort: "9050"
    }).then(onLoadConfig, onError);
}

function onLoadConfig(config) {
    toggleTor(config.enabled);
    addressInput.value = config.proxyAddress;
    portInput.value = config.proxyPort;

    proxySettings.socks = config.proxyAddress + ":" + config.proxyPort;
}

function listenForEnable() {
    enableTorCheckbox.addEventListener("change", function () {
        if (this.checked) {
            proxySettings.socks = addressInput.value + ":" + portInput.value; 

            browser.storage.local.set({
                enabled: true,
                proxyAddress: addressInput.value,
                proxyPort: portInput.value
            }).then(enableTor, onError);
        } else {
            browser.storage.local.set({
                enabled: false,
            }).then(disableTor, onError);
        }
    });
}

function toggleTor(isEnabled) {
    enableTorCheckbox.checked = isEnabled;
    addressInput.disabled = isEnabled;
    portInput.disabled = isEnabled;

    browser.browserAction.setIcon(isEnabled ? enabledIcon : disabledIcon);
}

function enableTor() {
    browser.proxy.settings.set({value: proxySettings}).then( success => {
        if (success)
            toggleTor(true)
    });
}
function disableTor() {
    browser.proxy.settings.clear({}).then( success => {
        if (success)
            toggleTor(false);
    });
}

function onError() {
    console.error("An error occured!");
}

window.onload = function () {
    loadConfig();
    listenForEnable();
};
